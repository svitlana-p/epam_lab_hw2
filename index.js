const fs = require("fs");
const path = require("path");
const express = require("express");
const morgan = require("morgan");
const app = express();
const mongoose = require("mongoose");

mongoose.connect(
  "mongodb+srv://SvitlanaPetrytska:epam2022@cluster0.22vwbrd.mongodb.net/app?retryWrites=true&w=majority"
);

const { notesRouter } = require("./src/notesRouter.js");
const { authRouter } = require("./src/authRouter.js");
const { authMiddleware } = require("./src/middleware/authMiddleware.js");
const { userRouter } = require("./src/userRouter.js");
const PORT = 8080;
//let __dirname;
const accessedLog = fs.createWriteStream(
  path.join(__dirname, "requestLogs.log"),
  { flags: "a" }
);

app.use(express.json());
app.use(morgan("tiny", { stream: accessedLog }));

app.use("/api/notes", authMiddleware, notesRouter);
app.use("/api/auth", authRouter);
app.use("/api/users", authMiddleware, userRouter);

const start = async () => {
  try {
    app.listen(PORT);
    console.log(`App started at port: ${PORT}`);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

//ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res) {
  console.error(err);
  res.status(500).send({ message: "Server error" });
}
