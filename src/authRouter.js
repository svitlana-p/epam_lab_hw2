const express = require("express");
const router = express.Router();
const { registerUser, loginUser } = require("./authService.js");
//const { authMiddleware } = require('./middleware/authMidleware');

router.post("/register", registerUser);

router.post("/login", loginUser);

module.exports = {
  authRouter: router,
};
