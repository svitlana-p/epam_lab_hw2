const express = require("express");
const router = express.Router();
const {
  createNote,
  getNotes,
  getNote,
  updateNote,
  checkNotes,
  deleteNote,
} = require("./notesService.js");

router.post("/", createNote);

router.get("/", getNotes);

router.patch("/:id", checkNotes);

router.put("/:id", updateNote);

router.get("/:id", getNote);

router.delete("/:id", deleteNote);

module.exports = {
  notesRouter: router,
};
