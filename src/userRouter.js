const express = require("express");
const router = express.Router();
const { getUser, editUser, deleteUser } = require("./userService.js");

router.get("/me", getUser);

router.patch("/me", editUser);

router.delete("/me", deleteUser);

module.exports = {
  userRouter: router,
};
