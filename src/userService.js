const { User } = require("./models/Users.js");
const bcrypt = require("bcryptjs");

const getUser = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user.userId });
    if (!user) {
      return next({ message: "Not authorired", status: 401 });
    }
    res.json({
      user: {
        _id: req.user.userId,
        username: user.username,
        createdDate: user.createdAt,
      },
    });
  } catch (err) {
    return {
      message: err,
    };
  }
};

const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndDelete(req.user.userId);
    return res.json({
      message: "Success",
    });
  } catch (err) {
    return {
      message: err,
    };
  }
};

const editUser = async (req, res, next) => {
  try {
    const user = await User.findById(req.user.userId);

    const password = user.password;

    let oldPassword = req.body.oldPassword;

    const newPassword = req.body.newPassword;

    if (!oldPassword)
      next({ message: "Please enter your password", status: 404 });
    if (!newPassword)
      next({ message: "Please enter new password", status: 404 });

    const isTrue = await bcrypt.compare(oldPassword, password);

    if (isTrue) {
      user.password = await bcrypt.hash(newPassword, 10);
      user.save();
      res.json({
        message: "Success",
      });
    }
  } catch (err) {
    return {
      message: err,
    };
  }
};

module.exports = {
  getUser,
  deleteUser,
  editUser,
};

// "oldPassword":"pass",
// "newPassword":"pass1"
// "username":"user1",
// "password":"pass"
