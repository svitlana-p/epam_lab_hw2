const { Note } = require("./models/Notes.js");

const createNote = (req, res, next) => {
  const { text } = req.body;
  const userId = req.user.userId;
  if (!text) next({ message: "Please, enter text", status: 404 });

  const note = new Note({
    text,
    userId,
  });
  note.save().then(() => {
    res.json({
      message: "Success",
    });
  });
};

const getNotes = (req, res) => {
  return Note.find({ userId: req.user.userId }, "-__v").then((notes) => {
    const { offset = 0, limit = 0 } = req.query;
    res.send({
      offset: offset,
      limit: limit,
      count: notes.length,
      notes,
    });
  });
};

const getNote = (req, res) => {
  return Note.findById(req.params.id).then((note) =>
    res.json({
      note,
    })
  );
};

const updateNote = async (req, res) => {
  const { text } = req.body;
  const note = await Note.findByIdAndUpdate(
    { _id: req.params.id, userId: req.user.userId },
    { $set: { text } }
  );

  return note.save().then(() => res.json({ message: "Success" }));
};

const checkNotes = async (req, res) => {
  const note = await Note.findById({
    _id: req.params.id,
    userId: req.user.userId,
  });

  note.completed = !note.completed;

  return note.save().then(() => res.json({ message: "Success" }));
};

const deleteNote = async (req, res) => {
  return await Note.findByIdAndDelete({
    _id: req.params.id,
    userId: req.user.userId,
  }).then(() => res.json({ message: "Success" }));
};

module.exports = {
  createNote,
  getNotes,
  getNote,
  updateNote,
  checkNotes,
  deleteNote,
};
